<?php

/**
 * @file
 *	Implement an API for autotaxonomy.
 *
 * The API handles creation, deletion and listing of
 * autotaxonomies. It also deals with saving, updating
 * and deleting terms through autotaxonomy.
 */

/**
 * autotaxonomy_api(): Public Access Function
 *
 * This is the main function of the api, and is the only
 * function that modules should call directly.
 *
 * @param $op
 *	An operator for the API. It should be one of the following:
 *	'create': Create a new autotaxonomy.
 *	'delete': Delete an autotaxonomy.
 *	'list': Return a list of autotaxonomies.
 *	'term_set': Tell autotaxonomy to create a new term when the module exits.
 *	'term_save': Tell autotaxonomy to create a new term now.
 *		Do not use this unless you know what you are doing.
 *	'term_update': Tell autotaxonomy to update a term.
 *	'term_destroy': Tell autotaxonomy to delete a term.
 *	'term_get': Request a term from autotaxonomy.
 * @return
 *	Varies, based on what action is being performed. Typically,
 * it will return TRUE if the action was a success, or FALSE
 * if it was not. Exceptions to this are 'list', which returns
 * a list of autotaxonomies, and 'term_get', which returns a
 * term.
 *
 */
function autotaxonomy_api($op, $edit = array()) {
	switch($op) {
		//Functions to deal with autotaxonomies
		case 'create':
			return _autotaxonomy_create($edit);
		case 'delete':
			return _autotaxonomy_delete($edit);
		case 'list':
			return _autotaxonomy_list();

		//Functions to deal with handling specific terms
		case 'term_set':
			return _autotaxonomy_term_set($edit);
		case 'term_save':
			return _autotaxonomy_term_save($edit);
		case 'term_update':
			return _autotaxonomy_term_update($edit);
		case 'term_destroy':
			return _autotaxonomy_term_destroy($edit);
		case 'term_get':
			return _autotaxonomy_term_get($edit);
		default:
			return FALSE;
	}
}

/**
 * Create a new autotaxonomy
 */
function  _autotaxonomy_create($edit) {
	return db_query("INSERT INTO {autotaxonomy} (type, vid, parent, autoupdate, autodelete, override) VALUES ('%s', %d, %d, %d, %d, %d)", $edit['type'], $edit['vocabulary'], $edit['parent'], $edit['automaintain'], $edit['autodelete'], $edit['override']);			
}

/**
 * Delete an existing autotaxonomy
 */
function _autotaxonomy_delete($edit) {
	return db_query("DELETE FROM {autotaxonomy} WHERE aid = %d", $edit['aid']);
}

/**
 * Return a list of existing autotaxonomies
 */
function _autotaxonomy_list() {
	$qs = db_query("SELECT * FROM {autotaxonomy} a");
	while($obj = db_fetch_object($qs)) {
		$autotaxonomies[$obj->type][] = $obj;
	}

	return $autotaxonomies;
}


/**
 * Get an autotaxonomy-created term relationship
 */
function _autotaxonomy_term_get($edit) {
	$qs = db_query("SELECT t.tid FROM {autotaxonomy_nid_tid} t WHERE t.nid = %d", $edit['nid']);
	$tid = db_fetch_array($qs);
	if($tid) {
		$tid = array_shift($tid);
	} else {
		return;
	}
	
	if(!empty($tid)) {
		$term = taxonomy_get_term($tid);
		return $term;
	} else {
		return FALSE;
	}
}

/**
 * Set an autotaxonomy-created term to be created when the module exits
 */
function _autotaxonomy_term_set($edit) {
	$term = $edit['term'];
	
	taxonomy_save_term($term);
	$qs = db_query_range("SELECT t.tid FROM {term_data} t ORDER BY t.tid DESC", 0, 1);
	$tid = db_fetch_object($qs);
	$tid = $tid->tid;
	
	//Associate the current node with that term.
	//$taxonomy_term = taxonomy_get_term_by_name($term['name']);
	
	$GLOBALS['autotaxonomy'][] = array(
		'nid' => $edit['nid'],
		'tid' => $tid,
	);

	return TRUE;
}

/**
 * Save an autotaxonomy-created term
 */
function _autotaxonomy_term_save($edit) {

	//Get the term based on the tid.
	$term = taxonomy_get_term($edit['tid']);
	
	//Get the vocabulary for this term.
	$vocabulary = taxonomy_get_vocabulary($term->vid);
	if(empty($vocabulary)) {
		return;
	}
	
	//If this vocabulary is not enabled for multiple selection, delete the old term association.
	if(!$vocabulary->multiple) {
		$old_tid = db_result(db_query("SELECT tn.tid FROM {term_node} tn INNER JOIN {term_data} td ON tn.tid=td.tid WHERE tn.nid = %d AND td.vid = %d", $edit['nid'], $vocabulary->vid));
		if(!empty($old_tid)) {
			db_query('DELETE FROM {term_node} WHERE nid = %d AND tid = %d', $edit['nid'], $old_tid);
		}
	}

	//Now, get all pre-existing term associations, so that we can preserve them.
	$terms = taxonomy_node_get_terms($edit['nid']);
	$tids = array();
	foreach(array_keys($terms) as $tid) {
		$tids[$tid] = $tid;
	}

	//Add the new tid to the node
	$tids[$edit['tid']] = $edit['tid'];
	$tids = array_unique($tids);

	//Do the association
	$success = taxonomy_node_save($edit['nid'], $tids);

	//If it worked, keep track of it.
	if($success) {
		db_query("INSERT INTO {autotaxonomy_nid_tid} (nid, tid) VALUES (%d, %d)", $edit['nid'], $edit['tid']);
	}

	return $success;
}

/**
 * Update an autotaxonomy-created term
 */
function _autotaxonomy_term_update($edit) {
	return db_query("UPDATE {term_data} SET name = '%s' WHERE tid = %d", $edit['name'], $edit['tid']);
}

/**
 * Delete an autotaxonomy-created term
 */
function _autotaxonomy_term_destroy($edit) {
	$qs = db_query("SELECT a.tid FROM {autotaxonomy_nid_tid} a WHERE a.nid = %d", $edit['nid']);
	while($obj = db_fetch_object($qs)) {
		//print "<pre>"; print_r($obj); print "</pre>";
		taxonomy_del_term($obj->tid);
	}
	
	db_query("DELETE FROM {autotaxonomy_nid_tid} WHERE nid = %d", $edit['nid']);
	
	return TRUE;;
}
